let pokemonTrainer = {
  name: "Ash Ketchum",
  age: 10,
  pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
  friends: {
      hoenn: ['May', 'Max'],
      kanto: ['Brock', 'Misty']
  },

  talk: function(){
    
     console.log(`${this.pokemon[0]}! I choose you!`) 
  },

}

console.log(pokemonTrainer);
console.log("Result of dot notation: ");
console.log(pokemonTrainer.name);
console.log("Result of bracket notation: ");
console.log(pokemonTrainer['pokemon']);
console.log("Result talk method: ");
pokemonTrainer.talk();


function Pokemon(name, level){
  //Propeties
  this.name = name;
  this.level = level;
  this.health = 2 * level;
  this.attack = level;
   
  //Method / Function
  this.tackle =  function(target){
    this.subtract = target.health - this.attack;
    target.health = this.subtract;
    console.log(`${this.name} tackled ${target.name}`);
    console.log(`${target.name}'s health is now to reduced to ${target.health}`);
    
    
    if(target.health <=0 ){
      this.faint(target);
    }

  }

  this.faint = function(pokemonName){
      console.log(`${pokemonName.name} fainted`);
  }

}


let pikachu = new Pokemon('Pikachu', 12);
let geodude = new Pokemon('Geodude', 8);
let mewtwo = new Pokemon('Mewtwo', 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu)
console.log(pikachu);

mewtwo.tackle(geodude)
console.log(geodude);

pikachu.tackle(mewtwo)
console.log(mewtwo);

pikachu.tackle(mewtwo)
console.log(mewtwo);

pikachu.tackle(mewtwo)
console.log(mewtwo);
pikachu.tackle(mewtwo)
